﻿namespace MaxExtensions
{
    using System;
    using System.Linq;

    public static class ArrayExtensions
    {
        public static T[][] Rotate<T>(this T[][] source)
        {
            int length = source[0].Length;
            T[][] retVal = new T[length][];
            for (int i = 0; i < length; i++)
            {
                retVal[i] = source.Select(arr => arr[i]).ToArray();
            }

            return retVal;
        }

        public static T[,] To2D<T>(this T[][] source)
        {
            try
            {
                var FirstDim = source.Length;
                var SecondDim =
                    source.GroupBy(row => row.Length).Single()
                        .Key; // throws InvalidOperationException if source is not rectangular

                var result = new T[FirstDim, SecondDim];
                for (var i = 0; i < FirstDim; ++i)
                    for (var j = 0; j < SecondDim; ++j)
                        result[i, j] = source[i][j];

                return result;
            }
            catch (InvalidOperationException)
            {
                throw new InvalidOperationException("The given jagged array is not rectangular.");
            }
        }
    }
}
