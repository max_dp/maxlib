﻿namespace MaxExtensions
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    public static class DataAnalysis
    {
        private const double DEFAULT_ALPHA = 0.05;

        /// <summary>
        /// Calculates correlation coefficient between two sets
        /// </summary>
        /// <param name="firstSet"></param>
        /// <param name="secondSet"></param>
        /// <returns></returns>
        public static double GetCorrelationCoefficient(this IEnumerable<double> firstSet, IEnumerable<double> secondSet)
        {
            double setsAverage = firstSet.Zip(secondSet, (x, y) => x * y).Average();

            return (setsAverage - firstSet.Average() * secondSet.Average()) /
                   (firstSet.StandartDeviation() * secondSet.StandartDeviation());
        }

        /// <summary>
        /// Compares means of two unequal sets
        /// </summary>
        /// <param name="firstSet">First set</param>
        /// <param name="secondSet">Second set</param>
        /// <param name="customAlpha">Alpha for Student quantile</param>
        /// <returns>True if two sets have equal means</returns>
        public static bool CompareUnequalSetMeansWithTCriteria(this IEnumerable<double> firstSet, IEnumerable<double> secondSet, out double tStatistic, double? customAlpha = null)
        {
            tStatistic = (firstSet.Average() - secondSet.Average()) / Math.Sqrt(
                firstSet.UnBiasedVariance() / firstSet.Count() +
                secondSet.UnBiasedVariance() / secondSet.Count());

            double p = 1 - (customAlpha ?? DEFAULT_ALPHA) /*/ 2*/;
            double v = firstSet.Count() + secondSet.Count() - 2;
            return Math.Abs(tStatistic) <= Quantile.t(p, v);
        }

        /// <summary>
        /// Compares means of two or more sets (using the F distribution)
        /// </summary>
        /// <param name="sets">IEnumerable of sets</param>
        /// <param name="customAlpha">Alpha for Fisher quantile</param>
        /// <returns>True if sets are equal</returns>
        public static bool CompareWithOneWayVarienceAnalysis(IEnumerable<IEnumerable<double>> sets, out double fStatistic, double? customAlpha = null)
        {
            double overallCount = sets.Sum(s => s.Count());
            double overallAverage = sets.Sum(s => s.Count() * s.Average()) / overallCount;

            double sb = sets.Sum(s => s.Count() * Math.Pow(s.Average() - overallAverage, 2)) / (sets.Count() - 1);
            double sw = sets.Sum(s => (s.Count() - 1) * s.UnBiasedVariance()) / (overallCount - sets.Count());

            fStatistic = sb / sw;
            double p = 1 - (customAlpha ?? DEFAULT_ALPHA);
            double v1 = sets.Count() - 1;
            double v2 = overallCount - sets.Count();
            return Math.Abs(fStatistic) <= Quantile.f(p, v1, v2);
        }

        public static bool GetMultipleCorrelationSignificance(double[][] correlationMatrix, 
                                                              int featureNumber,
                                                              int objectsCount,
                                                              int featuresCount,
                                                              double? customAlpha = null)
        {
            var minorMatrix = correlationMatrix.Where((_, i) => i != featureNumber)
                                                   .Select(l => l.Where((_, i) => i != featureNumber).ToArray())
                                                   .ToArray();

            double matrixDet = alglib.rmatrixdet(correlationMatrix.To2D());
            double minorDet = alglib.rmatrixdet(minorMatrix.To2D());
            double coef = Math.Sqrt(1 - matrixDet / minorDet);
            double statistic = (Math.Pow(coef, 2) * (objectsCount - featuresCount)) /
                               ((featuresCount - 1) * (1 - Math.Pow(coef, 2)));
            Debug.WriteLine($"Statistic: {statistic}");

            double p = 1 - (customAlpha ?? DEFAULT_ALPHA);
            double v1 = featuresCount - 1;
            double v2 = objectsCount - featuresCount;
            return statistic <= Quantile.f(p, v1, v2);
        }

        public static int GetSEMClustersCount(IEnumerable<double> set, out double bic, int maxClustersCounts = 4, int minObjsInCluster = 5)
        {
            const double EPSILON = 0.03;//0.001;//0.0001;
            int clustersCount = 0;
            Random r = new Random();
            double[][] gMatrixBackup; double diff = 0; double likehood = 0;

            double[][] gMatrix = set.Select(_ =>
            {
                var randomNumbers = Enumerable.Repeat(0.0, maxClustersCounts)
                                              .Select(__ => r.NextDouble())
                                              .ToArray();
                return randomNumbers.Select(n => n / randomNumbers.Sum()).ToArray();
            }).ToArray();

            do
            {
                //S-step
                var clusteredObjects = set.Select((o, i) =>
                {
                    int? randomCluster = null;
                    double rand = r.NextDouble();

                    for (int j = 0; j < gMatrix[i].Length; j++)
                    {
                        if (rand <= gMatrix[i][j])
                        {
                            randomCluster = j;
                            break;
                        }
                        else
                        {
                            rand -= gMatrix[i][j];
                        }
                    }
                    return new { Value = o, Cluster = randomCluster ?? throw new InvalidOperationException() };
                });

                var cachedClusteredObjects = clusteredObjects.ToArray();
                var littleClusters = gMatrix.First().Select((_, i) => i).Where(i => 
                {
                    var clusterValues = cachedClusteredObjects.Where(o => o.Cluster == i).Select(o => o.Value);
                    return clusterValues.Count() < minObjsInCluster || clusterValues.Distinct().Count() <= 1;
                }).ToArray();
                while (littleClusters.Count() > 0)
                {
                    int? littleCluster = littleClusters.FirstOrDefault();
                    for (int i = 0; i < gMatrix.Length; i++)
                    {
                        var newRow = gMatrix[i].Where((_, j) => j != (littleCluster ?? throw new InvalidOperationException()));
                        double sum = newRow.Sum();
                        gMatrix[i] = newRow.Select(n => n / sum).ToArray();
                    }
                    //TODO: Refactor this awfull code
                    cachedClusteredObjects = clusteredObjects.ToArray();
                    littleClusters = gMatrix.First().Select((_, i) => i).Where(i =>
                    {
                        var clusterValues = cachedClusteredObjects.Where(o => o.Cluster == i).Select(o => o.Value);
                        return clusterValues.Count() < minObjsInCluster || clusterValues.Distinct().Count() <= 1;
                    }).ToArray();
                }
                
                //M-step
                var clusters = cachedClusteredObjects.GroupBy(o => o.Cluster, (clusterNum, objs) => 
                {
                    var values = objs.Select(o => o.Value).ToArray();
                    double m = values.Average();
                    double sigma = values.StandartDeviation();//.UnBiasedVariance();

                    return new { Cluster = clusterNum, P = (double)values.Length / set.Count(), F = NormalDistribution.f(sigma, m) };
                }).ToArray();
                clustersCount = clusters.Length;

                //E-step
                likehood = 0;
                gMatrixBackup = gMatrix.Select(row => row.ToArray()).ToArray();
                for (int i = 0; i < gMatrix.Length; i++)
                {
                    var obj = cachedClusteredObjects[i];
                    var denominator = clusters.Sum(c => c.P * c.F(obj.Value));
                    likehood += Math.Log(denominator);
                    for (int l = 0; l < gMatrix[i].Length; l++)
                    {
                        gMatrix[i][l] = clusters[l].P * clusters[l].F(obj.Value) / denominator;
                    }
                }

                diff = gMatrix.Zip(gMatrixBackup, (firstRow, secondRow) => firstRow.Zip(secondRow, (v1, v2) => Math.Abs(v1 - v2)).Max()).Max();
            } while (diff >= EPSILON && clustersCount > 1);

            bic = (2 + (clustersCount > 1 ? 1 : 0)) * clustersCount * Math.Log(set.Count()) - 2 * likehood;
            return clustersCount;
        }

        public static double CalculateBICForSet(IEnumerable<double> set)
        {
            double m = set.Average();
            double sigma = set.StandartDeviation();//.UnBiasedVariance();

            double likehood = set.Sum(x => Math.Log(NormalDistribution.f(sigma, m)(x)));
            return 2 * Math.Log(set.Count()) - 2 * likehood;
        }

        #region Privates
        
        private static class Quantile
        {
            /// <summary>
            /// Student Quantile
            /// </summary>
            /// <param name="p"></param>
            /// <param name="v"></param>
            /// <returns></returns>
            public static double t(double p, double v)
            {
                double Up = U(p);
                return Up + ((1 / (v * 4)) * (Math.Pow(Up, 3) + Up)) +
                       ((1 / (Math.Pow(v, 2) * 96)) * (5 * Math.Pow(Up, 5) + 16 * Math.Pow(Up, 3) + 3 * Up)) // Up + g1 + g2
                       + ((1 / (Math.Pow(v, 3) * 384)) * (3 * Math.Pow(Up, 7) + 19 * Math.Pow(Up, 5) + 17 * Math.Pow(Up, 3) - 15 * Up)) //g3
                       + ((1 / (Math.Pow(v, 4) * 92160)) * (79 * Math.Pow(Up, 9) + 779 * Math.Pow(Up, 7) + 1482 * Math.Pow(Up, 5) - 1920 * Math.Pow(Up, 3) - 945 * Up));
            }

            /// <summary>
            /// Fisher Quantile
            /// </summary>
            /// <param name="p"></param>
            /// <param name="v1"></param>
            /// <param name="v2"></param>
            /// <returns></returns>
            public static double f(double p, double v1, double v2)
            {
                double Up = U(p);
                double sigma = (1 / v1) + (1 / v2);
                double delta = (1 / v1) - (1 / v2);
                double z = Up * Math.Sqrt(sigma / 2) - (delta * (Math.Pow(Up, 2) + 2) / 6)
                    + Math.Sqrt(sigma / 2) * ((sigma / 24) * (Math.Pow(Up, 2) + 3 * Up) + (Math.Pow(delta, 2) * (Math.Pow(Up, 3) + 11 * Up) / (72 * sigma)))
                    - sigma * delta * (Math.Pow(Up, 4) + 9 * Math.Pow(Up, 2) + 8) / 120 + Math.Pow(delta, 3) * (3 * Math.Pow(Up, 4) + 7 * Math.Pow(Up, 2) - 16) / (3240 * sigma)
                    + Math.Sqrt(sigma / 2) * (Math.Pow(sigma, 2) * (Math.Pow(Up, 5) + 20 * Math.Pow(Up, 3) + 15 * Up) / 1920
                    + Math.Pow(delta, 4) * (Math.Pow(Up, 5) + 44 * Math.Pow(Up, 3) + 183 * Up) / 2880
                    + Math.Pow(delta, 4) * (9 * Math.Pow(Up, 5) - 284 * Math.Pow(Up, 3) - 1513 * Up) / (155520 * Math.Pow(sigma, 2)));

                return Math.Exp(2 * z);
            }

            /// <summary>
            /// Normal Distribution Quantile
            /// </summary>
            /// <param name="p"></param>
            /// <returns></returns>
            private static double U(double p)
            {
                if (p <= 0.5)
                {
                    double t = Math.Sqrt(-2 * Math.Log(p, Math.E));

                    return ((2.515517 + 0.802853 * t + 0.010328 * Math.Pow(t, 2)) / (1 + 1.432788 * t + 0.1892659 * Math.Pow(t, 2) + 0.001308 * Math.Pow(t, 3))) - t;
                }
                else
                {
                    double t = Math.Sqrt(-2 * Math.Log(1 - p, Math.E));

                    return t - ((2.515517 + 0.802853 * t + 0.010328 * Math.Pow(t, 2)) / (1 + 1.432788 * t + 0.1892659 * Math.Pow(t, 2) + 0.001308 * Math.Pow(t, 3)));
                }
            }
        }

        private static class NormalDistribution
        {
            /// <summary>
            /// Density function of normal distribution
            /// </summary>
            /// <param name="sigma"></param>
            /// <param name="m"></param>
            /// <returns></returns>
            public static Func<double, double> f(double sigma, double m) =>
                (double x) =>
                    (1 / Math.Sqrt(2 * Math.PI * sigma))
                    * Math.Pow(Math.E, -Math.Pow(x - m, 2) / (2 * Math.Pow(sigma, 2)));
        }
        
        #endregion
    }
}
