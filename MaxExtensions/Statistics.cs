﻿namespace MaxExtensions
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public static class Statistics
    {
        /// <summary>
        /// Computes the median of a sequence of <see cref="double"/> values
        /// </summary>
        /// <exception cref="ArgumentNullException" />
        /// <exception cref="InvalidOperationException" />
        public static double Median(this IEnumerable<double> source)
        {
            if (source == null) throw new ArgumentNullException(/*nameof(source)*/"source");

            double[] array = source as double[] ?? source.ToArray();
            Array.Sort(array);
            int count = array.Length;
            if (count > 0) return count % 2 == 0 ?
                                  (array[count / 2 - 1] + array[count / 2]) / 2 :
                                  array[(count + 1) / 2 - 1];

            throw new InvalidOperationException("Collection is empty");
        }

        public static double StandartDeviation(this IEnumerable<double> source)
        {
            return Math.Sqrt(source.UnBiasedVariance());
        }

        public static double UnBiasedVariance(this IEnumerable<double> source)
        {
            if (source == null) throw new ArgumentNullException(/*nameof(source)*/"source");

            double[] array = source as double[] ?? source.ToArray();
            double average = array.Average();
            double sum = array.Sum(value => Math.Pow(value - average, 2));

            if (array.Length > 0) return sum / (array.Length - 1);

            throw new InvalidOperationException("Collection is empty");
        }

        public static double BiasedVariance(this IEnumerable<double> source)
        {
            double[] array = source as double[] ?? source.ToArray();

            return array.Average(x => Math.Pow(x, 2)) - Math.Pow(array.Average(), 2);
        }

        public static double BiasedAssimetryCoef(this IEnumerable<double> source)
        {
            double[] array = source as double[] ?? source.ToArray();
            double average = array.Average();
            double sum = array.Sum(x => Math.Pow(x - average, 3));

            return 1 / (array.Length * Math.Pow(array.BiasedVariance(), (double)3 / 2)) * sum;
        }

        public static double UnBiasedAssimetryCoef(this IEnumerable<double> source)
        {
            double[] array = source as double[] ?? source.ToArray();
            var length = array.Length;

            return Math.Sqrt(length * (length - 1)) / (length - 2) * array.BiasedAssimetryCoef();
        }

        public static double BiasedKurtosis(this IEnumerable<double> source)
        {
            double[] array = source as double[] ?? source.ToArray();
            double average = array.Average();
            double sum = array.Sum(x => Math.Pow(x - average, 4));

            return 1 / (array.Length * Math.Pow(array.BiasedVariance(), 2)) * sum;
        }

        public static double UnBiasedKurtosis(this IEnumerable<double> source)
        {
            double[] array = source as double[] ?? source.ToArray();
            var length = array.Length;

            return (Math.Pow(length, 2) - 1) / ((length - 2) * (length - 3)) * (array.BiasedKurtosis() - 3 + (double)6 / (length + 1));
        }

    }
}
